# README 
#
# 1.{train_file} and {val_file} are the output files of get_feature.sh(for {is_train=1}). The data format of it should be:
# label(for video) feature_number1:feature_value1 feature_number2:feature_value2 ...
# for example:
# 0 1:0.694165 2:0.46501 3:0.464049 4:0.460526 5:0.460396 6:0.433268 7:0.429479 8:0.428869 9:0.41955 10:0.419032 11:0.416854 ...
# 1 1:0.999982 2:0.999981 3:0.99998 4:0.99998 5:0.99998 6:0.99998 7:0.99998 8:0.99998 9:0.99998 10:0.99998 11:0.99998 ...
#
# README END





conf_weights=/home/alex/crowdcount/yolov4/YOLOv4-P7/ScaledYOLOv4/runs/exp92_ch-wp2k_CEP_high2k3_updown/weights/best.pt
cfg_out=/home/alex/crowdcount/yolov4/YOLOv4-P7/ScaledYOLOv4_v2/models/yolov4-csp_class2.cfg
img_size=320
data_out=/home/alex/crowdcount/yolov4/YOLOv4-P7/ScaledYOLOv4_v2/data/person_count_mrttestv3_topdown_bigimg800.yaml





#for iou_thres in 0.05 0.1 0.2 0.3 0.4 0.5
#for iou_thres in 0.3
#for iou_thres in 0.02 0.04 0.06 0.08 0.1 1.02
#for iou_thres in 0.3 0.32 0.34 0.36 0.38 0.4 0.42 0.44 0.46 0.48 0.5
for iou_thres in 0.1 0.12 0.14 0.16 0.18 0.2 0.22 0.24 0.26 0.28 0.3
do


    #for conf_thre in 0.05 0.1 0.2 0.3 0.4 0.5
    #for conf_thre in 0.02 0.04 0.06 0.08 0.1 1.02
    #for conf_thre in 0.1 0.2 0.3 0.4 0.5 0.6
    #for conf_thre in 0.3
    #for conf_thre in 0.5 0.51 0.52 0.53 0.54 0.55 0.56 0.57 0.58 0.59 0.6 0.61 0.62 0.63 0.64 0.65 0.66 0.67 0.68 0.69
    #for conf_thre in 0.4 0.41 0.42 0.43 0.44 0.45 0.46 0.47 0.48 0.49 0.5 0.51 0.52 0.53 0.54 0.55 0.56 0.57 0.58 0.59 0.6
    #for conf_thre in 0.3 0.31 0.32 0.33 0.34 0.35 0.36 0.37 0.38 0.39 0.4 0.41 0.42 0.43 0.44 0.45 0.46 0.47 0.48 0.49 0.5
    #for conf_thre in 0.42 0.43 0.44 0.45 0.46
    #for conf_thre in 0.2 0.21 0.22 0.23 0.24 0.25 0.26 0.27 0.28 0.29 0.3 0.31 0.32 0.33 0.34 0.35 0.36 0.37 0.38 0.39 0.4
    for conf_thre in 0.1 0.12 0.14 0.16 0.18 0.2 0.22 0.24 0.26 0.28 0.3

    do



        python test_formrt_morecompare_testv2_nopad_default.py --img $img_size --conf $conf_thre --batch 1 --device 0 --data /home/alex/crowdcount/yolov4/YOLOv4-P7/ScaledYOLOv4_v2/data/person_count_body.yaml --cfg $cfg_out --weights $conf_weights --iou-thres $iou_thres  --data $data_out
        #python test_formrt_nopad.py --img $img_size --conf $conf_thre --batch 1 --device 0 --data /home/alex/crowdcount/yolov4/YOLOv4-P7/ScaledYOLOv4_v2/data/person_count_body.yaml --cfg $cfg_out --weights $conf_weights --iou-thres $iou_thres  --data $data_out



    done


done
