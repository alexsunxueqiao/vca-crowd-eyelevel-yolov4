# README 
#
# 1.{train_file} and {val_file} are the output files of get_feature.sh(for {is_train=1}). The data format of it should be:
# label(for video) feature_number1:feature_value1 feature_number2:feature_value2 ...
# for example:
# 0 1:0.694165 2:0.46501 3:0.464049 4:0.460526 5:0.460396 6:0.433268 7:0.429479 8:0.428869 9:0.41955 10:0.419032 11:0.416854 ...
# 1 1:0.999982 2:0.999981 3:0.99998 4:0.99998 5:0.99998 6:0.99998 7:0.99998 8:0.99998 9:0.99998 10:0.99998 11:0.99998 ...
#
# README END





conf_weights=/home/alex/crowdcount/yolov4/YOLOv4-P7/ScaledYOLOv4/runs/exp53_ch-wp2k-obj5e2/weights/best.pt
cfg_out=/home/alex/crowdcount/yolov4/YOLOv4-P7/ScaledYOLOv4_v2/models/yolov4-csp_class2.cfg
#img_size=640
data_out=/home/alex/crowdcount/yolov4/YOLOv4-P7/ScaledYOLOv4_v2/data/person_count.yaml




#for iou_thres in 0.5 0.6 0.7
for img_size in 320 360 400 440 480 520 560 600 640
do


    #for conf_thre in 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9
    #for conf_thre in 0.3 0.4 0.5 0.6
    #for conf_thre in 0.5 0.51 0.52 0.53 0.54 0.55 0.56 0.57 0.58 0.59 0.6 0.61 0.62 0.63 0.64 0.65 0.66 0.67 0.68 0.69
    #for conf_thre in 0.4 0.41 0.42 0.43 0.44 0.45 0.46 0.47 0.48 0.49 0.5 0.51 0.52 0.53 0.54 0.55 0.56 0.57 0.58 0.59 0.6
    #for conf_thre in 0.3 0.31 0.32 0.33 0.34 0.35 0.36 0.37 0.38 0.39 0.4 0.41 0.42 0.43 0.44 0.45 0.46 0.47 0.48 0.49 0.5
    #for conf_thre in 0.42 0.43 0.44 0.45 0.46
    for conf_thre in 0.3

    do



        #python test_formrt_morecompare_testv2.py --img $img_size --conf $conf_thre --batch 1 --device 0 --data /home/alex/crowdcount/yolov4/YOLOv4-P7/ScaledYOLOv4_v2/data/person_count_body.yaml --cfg $cfg_out --weights $conf_weights --iou-thres $iou_thres  --data $data_out
        python test_formrt.py --img $img_size --conf 0.5 --batch 1 --device 0 --data /home/alex/crowdcount/yolov4/YOLOv4-P7/ScaledYOLOv4_v2/data/person_count_body.yaml --cfg $cfg_out --weights $conf_weights --iou-thres 0.4  --data $data_out



    done


done
