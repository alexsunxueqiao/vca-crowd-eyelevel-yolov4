how to train
python train_lowestloss.py --device 0 --batch-size 48 --data ./data/person_count.yaml --cfg ./models/yolov4-csp_class2_mishtoswish.cfg --weights './yolov4-csp.weights' --name yolov4-eyelevel --img-size 320 --hyp ./data/hyp.scratch_saved5e2.yaml

how to test after training
sh test_multi_thres.sh