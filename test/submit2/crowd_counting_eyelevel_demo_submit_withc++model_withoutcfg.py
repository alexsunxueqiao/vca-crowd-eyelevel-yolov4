import torch
import argparse
from torch import nn
import torch.nn.functional as F
import os
import numpy as np
import cv2
import torchvision
import time
import numpy
import glob



def select_device(device='', batch_size=None):
    # device = 'cpu' or '0' or '0,1,2,3'
    cpu_request = device.lower() == 'cpu'
    if device and not cpu_request:  # if device requested other than 'cpu'
        os.environ['CUDA_VISIBLE_DEVICES'] = device  # set environment variable
        assert torch.cuda.is_available(), 'CUDA unavailable, invalid device %s requested' % device  # check availablity

    cuda = False if cpu_request else torch.cuda.is_available()
    if cuda:
        c = 1024 ** 2  # bytes to MB
        ng = torch.cuda.device_count()
        if ng > 1 and batch_size:  # check that batch_size is compatible with device_count
            assert batch_size % ng == 0, 'batch-size %g not multiple of GPU count %g' % (batch_size, ng)
        x = [torch.cuda.get_device_properties(i) for i in range(ng)]
        s = 'Using CUDA '
        for i in range(0, ng):
            if i == 1:
                s = ' ' * len(s)
            print("%sdevice%g _CudaDeviceProperties(name='%s', total_memory=%dMB)" %
                  (s, i, x[i].name, x[i].total_memory / c))
    else:
        print('Using CPU')

    print('')  # skip a line
    return torch.device('cuda:0' if cuda else 'cpu')


class Darknet(nn.Module):
    # YOLOv3 object detection model

    def __init__(self, cfg, img_size=(416, 416), verbose=False):
        super(Darknet, self).__init__()

        self.module_defs = parse_model_cfg(cfg)
        #print(self.module_defs)
        self.module_list, self.routs = create_modules(self.module_defs, img_size, cfg)
        self.yolo_layers = get_yolo_layers(self)
        # torch_utils.initialize_weights(self)

        # Darknet Header https://github.com/AlexeyAB/darknet/issues/2914#issuecomment-496675346
        self.version = np.array([0, 2, 5], dtype=np.int32)  # (int32) version info: major, minor, revision
        self.seen = np.array([0], dtype=np.int64)  # (int64) number of images seen during training
        #self.info(verbose) if not ONNX_EXPORT else None  # print model description

    def forward(self, x, augment=False, verbose=False):
        yolo_out, out = [], []
        #if verbose:
        #    print('0', x.shape)
        #    str = ''

        for i, module in enumerate(self.module_list):
            name = module.__class__.__name__
            if name in ['WeightedFeatureFusion', 'FeatureConcat', 'FeatureConcat2', 'FeatureConcat3', 'FeatureConcat_l']:  # sum, concat
                #if verbose:
                #    l = [i - 1] + module.layers  # layers
                #    sh = [list(x.shape)] + [list(out[i].shape) for i in module.layers]  # shapes
                #    str = ' >> ' + ' + '.join(['layer %g %s' % x for x in zip(l, sh)])
                x = module(x, out)  # WeightedFeatureFusion(), FeatureConcat()
            elif name == 'YOLOLayer':
                yolo_out.append(module(x, out))
            else:  # run module directly, i.e. mtype = 'convolutional', 'upsample', 'maxpool', 'batchnorm2d' etc.
                x = module(x)

            out.append(x if self.routs[i] else [])
            #if verbose:
            #    print('%g/%g %s -' % (i, len(self.module_list), name), list(x.shape), str)
            #    str = ''


        x, p = zip(*yolo_out)  # inference output, training output
        x = torch.cat(x, 1)  # cat yolo outputs

        return x, p

    def fuse(self):
        # Fuse Conv2d + BatchNorm2d layers throughout model
        print('Fusing layers...')
        fused_list = nn.ModuleList()
        for a in list(self.children())[0]:
            if isinstance(a, nn.Sequential):
                for i, b in enumerate(a):
                    if isinstance(b, nn.modules.batchnorm.BatchNorm2d):
                        # fuse this bn layer with the previous conv2d layer
                        conv = a[i - 1]
                        fused = torch_utils.fuse_conv_and_bn(conv, b)
                        a = nn.Sequential(fused, *list(a.children())[i + 1:])
                        break
            fused_list.append(a)
        self.module_list = fused_list
        #self.info() if not ONNX_EXPORT else None  # yolov3-spp reduced from 225 to 152 layers

    #def info(self, verbose=False):
    #    model_info(self, verbose)


def parse_model_cfg(path):
    # Parse the yolo *.cfg file and return module definitions path may be 'cfg/yolov3.cfg', 'yolov3.cfg', or 'yolov3'
    if not path.endswith('.cfg'):  # add .cfg suffix if omitted
        path += '.cfg'
    if not os.path.exists(path) and os.path.exists('cfg' + os.sep + path):  # add cfg/ prefix if omitted
        path = 'cfg' + os.sep + path

    with open(path, 'r') as f:
        lines = f.read().split('\n')
    lines = [x for x in lines if x and not x.startswith('#')]
    lines = [x.rstrip().lstrip() for x in lines]  # get rid of fringe whitespaces
    mdefs = []  # module definitions
    for line in lines:
        if line.startswith('['):  # This marks the start of a new block
            mdefs.append({})
            mdefs[-1]['type'] = line[1:-1].rstrip()
            if mdefs[-1]['type'] == 'convolutional':
                mdefs[-1]['batch_normalize'] = 0  # pre-populate with zeros (may be overwritten later)
        else:
            key, val = line.split("=")
            key = key.rstrip()

            if key == 'anchors':  # return nparray
                mdefs[-1][key] = np.array([float(x) for x in val.split(',')]).reshape((-1, 2))  # np anchors
            elif (key in ['from', 'layers', 'mask']) or (key == 'size' and ',' in val):  # return array
                mdefs[-1][key] = [int(x) for x in val.split(',')]
            else:
                val = val.strip()
                if val.isnumeric():  # return int or float
                    mdefs[-1][key] = int(val) if (int(val) - float(val)) == 0 else float(val)
                else:
                    mdefs[-1][key] = val  # return string

    # Check all fields are supported
    supported = ['type', 'batch_normalize', 'filters', 'size', 'stride', 'pad', 'activation', 'layers', 'groups',
                 'from', 'mask', 'anchors', 'classes', 'num', 'jitter', 'ignore_thresh', 'truth_thresh', 'random',
                 'stride_x', 'stride_y', 'weights_type', 'weights_normalization', 'scale_x_y', 'beta_nms', 'nms_kind',
                 'iou_loss', 'iou_normalizer', 'cls_normalizer', 'iou_thresh']

    f = []  # fields
    for x in mdefs[1:]:
        [f.append(k) for k in x if k not in f]
    u = [x for x in f if x not in supported]  # unsupported fields
    assert not any(u), "Unsupported fields %s in %s. See https://github.com/ultralytics/yolov3/issues/631" % (u, path)

    return mdefs


def create_modules(module_defs, img_size, cfg):
    # Constructs module list of layer blocks from module configuration in module_defs

    img_size = [img_size] * 2 if isinstance(img_size, int) else img_size  # expand if necessary
    _ = module_defs.pop(0)  # cfg training hyperparams (unused)
    output_filters = [3]  # input channels
    module_list = nn.ModuleList()
    routs = []  # list of layers which rout to deeper layers
    yolo_index = -1

    for i, mdef in enumerate(module_defs):
        modules = nn.Sequential()

        if mdef['type'] == 'convolutional':
            bn = mdef['batch_normalize']
            filters = mdef['filters']
            k = mdef['size']  # kernel size
            stride = mdef['stride'] if 'stride' in mdef else (mdef['stride_y'], mdef['stride_x'])
            if isinstance(k, int):  # single-size conv
                modules.add_module('Conv2d', nn.Conv2d(in_channels=output_filters[-1],
                                                       out_channels=filters,
                                                       kernel_size=k,
                                                       stride=stride,
                                                       padding=k // 2 if mdef['pad'] else 0,
                                                       groups=mdef['groups'] if 'groups' in mdef else 1,
                                                       bias=not bn))
            else:  # multiple-size conv
                modules.add_module('MixConv2d', MixConv2d(in_ch=output_filters[-1],
                                                          out_ch=filters,
                                                          k=k,
                                                          stride=stride,
                                                          bias=not bn))

            if bn:
                modules.add_module('BatchNorm2d', nn.BatchNorm2d(filters, momentum=0.03, eps=1E-4))
            else:
                routs.append(i)  # detection output (goes into yolo layer)

            if mdef['activation'] == 'leaky':  # activation study https://github.com/ultralytics/yolov3/issues/441
                modules.add_module('activation', nn.LeakyReLU(0.1, inplace=True))
            elif mdef['activation'] == 'swish':
                modules.add_module('activation', Swish())
            elif mdef['activation'] == 'mish':
                modules.add_module('activation', Mish())

        #elif mdef['type'] == 'deformableconvolutional':
        #    bn = mdef['batch_normalize']
        #    filters = mdef['filters']
        #    k = mdef['size']  # kernel size
        #    stride = mdef['stride'] if 'stride' in mdef else (mdef['stride_y'], mdef['stride_x'])
        #    if isinstance(k, int):  # single-size conv
        #        modules.add_module('DeformConv2d', DeformConv2d(output_filters[-1],
        #                                               filters,
        #                                               kernel_size=k,
        #                                               padding=k // 2 if mdef['pad'] else 0,
        #                                               stride=stride,
        #                                               bias=not bn,
        #                                               modulation=True))
        #    else:  # multiple-size conv
        #        modules.add_module('MixConv2d', MixConv2d(in_ch=output_filters[-1],
        #                                                  out_ch=filters,
        #                                                  k=k,
        #                                                  stride=stride,
        #                                                  bias=not bn))
        #
        #    if bn:
        #        modules.add_module('BatchNorm2d', nn.BatchNorm2d(filters, momentum=0.03, eps=1E-4))
        #    else:
        #        routs.append(i)  # detection output (goes into yolo layer)
        #
        #    if mdef['activation'] == 'leaky':  # activation study https://github.com/ultralytics/yolov3/issues/441
        #        modules.add_module('activation', nn.LeakyReLU(0.1, inplace=True))
        #    elif mdef['activation'] == 'swish':
        #        modules.add_module('activation', Swish())
        #    elif mdef['activation'] == 'mish':
        #        modules.add_module('activation', Mish())

        #elif mdef['type'] == 'BatchNorm2d':
        #    filters = output_filters[-1]
        #    modules = nn.BatchNorm2d(filters, momentum=0.03, eps=1E-4)
        #    if i == 0 and filters == 3:  # normalize RGB image
        #        # imagenet mean and var https://pytorch.org/docs/stable/torchvision/models.html#classification
        #        modules.running_mean = torch.tensor([0.485, 0.456, 0.406])
        #        modules.running_var = torch.tensor([0.0524, 0.0502, 0.0506])

        elif mdef['type'] == 'maxpool':
            k = mdef['size']  # kernel size
            stride = mdef['stride']
            maxpool = nn.MaxPool2d(kernel_size=k, stride=stride, padding=(k - 1) // 2)
            if k == 2 and stride == 1:  # yolov3-tiny
                modules.add_module('ZeroPad2d', nn.ZeroPad2d((0, 1, 0, 1)))
                modules.add_module('MaxPool2d', maxpool)
            else:
                modules = maxpool

        elif mdef['type'] == 'upsample':
            modules = nn.Upsample(scale_factor=mdef['stride'])

        elif mdef['type'] == 'route':  # nn.Sequential() placeholder for 'route' layer
            layers = mdef['layers']
            filters = sum([output_filters[l + 1 if l > 0 else l] for l in layers])
            routs.extend([i + l if l < 0 else l for l in layers])
            modules = FeatureConcat(layers=layers)

        #elif mdef['type'] == 'route2':  # nn.Sequential() placeholder for 'route' layer
        #    layers = mdef['layers']
        #    filters = sum([output_filters[l + 1 if l > 0 else l] for l in layers])
        #    routs.extend([i + l if l < 0 else l for l in layers])
        #    modules = FeatureConcat2(layers=layers)

        #elif mdef['type'] == 'route3':  # nn.Sequential() placeholder for 'route' layer
        #    layers = mdef['layers']
        #    filters = sum([output_filters[l + 1 if l > 0 else l] for l in layers])
        #    routs.extend([i + l if l < 0 else l for l in layers])
        #    modules = FeatureConcat3(layers=layers)

        #elif mdef['type'] == 'route_lhalf':  # nn.Sequential() placeholder for 'route' layer
        #    layers = mdef['layers']
        #    filters = sum([output_filters[l + 1 if l > 0 else l] for l in layers])//2
        #    routs.extend([i + l if l < 0 else l for l in layers])
        #    modules = FeatureConcat_l(layers=layers)

        elif mdef['type'] == 'shortcut':  # nn.Sequential() placeholder for 'shortcut' layer
            layers = mdef['from']
            filters = output_filters[-1]
            routs.extend([i + l if l < 0 else l for l in layers])
            modules = WeightedFeatureFusion(layers=layers, weight='weights_type' in mdef)

        #elif mdef['type'] == 'reorg3d':  # yolov3-spp-pan-scale
        #    pass

        elif mdef['type'] == 'yolo':
            yolo_index += 1
            stride = [8, 16, 32, 64, 128]  # P3, P4, P5, P6, P7 strides
            if any(x in cfg for x in ['yolov4-tiny', 'fpn', 'yolov3']):  # P5, P4, P3 strides
                stride = [32, 16, 8]
            layers = mdef['from'] if 'from' in mdef else []
            modules = YOLOLayer(anchors=mdef['anchors'][mdef['mask']],  # anchor list
                                nc=mdef['classes'],  # number of classes
                                img_size=img_size,  # (416, 416)
                                yolo_index=yolo_index,  # 0, 1, 2...
                                layers=layers,  # output layers
                                stride=stride[yolo_index])

            # Initialize preceding Conv2d() bias (https://arxiv.org/pdf/1708.02002.pdf section 3.3)
            try:
                j = layers[yolo_index] if 'from' in mdef else -1
                bias_ = module_list[j][0].bias  # shape(255,)
                bias = bias_[:modules.no * modules.na].view(modules.na, -1)  # shape(3,85)
                #bias[:, 4] += -4.5  # obj
                bias[:, 4] += math.log(8 / (640 / stride[yolo_index]) ** 2)  # obj (8 objects per 640 image)
                bias[:, 5:] += math.log(0.6 / (modules.nc - 0.99))  # cls (sigmoid(p) = 1/nc)
                module_list[j][0].bias = torch.nn.Parameter(bias_, requires_grad=bias_.requires_grad)
            except:
                print('WARNING: smart bias initialization failure.')

        else:
            print('Warning: Unrecognized Layer Type: ' + mdef['type'])

        # Register module list and number of output filters
        module_list.append(modules)
        output_filters.append(filters)

    routs_binary = [False] * (i + 1)
    for i in routs:
        routs_binary[i] = True
    return module_list, routs_binary

class WeightedFeatureFusion(nn.Module):  # weighted sum of 2 or more layers https://arxiv.org/abs/1911.09070
    def __init__(self, layers, weight=False):
        super(WeightedFeatureFusion, self).__init__()
        self.layers = layers  # layer indices
        self.weight = weight  # apply weights boolean
        self.n = len(layers) + 1  # number of layers
        if weight:
            self.w = nn.Parameter(torch.zeros(self.n), requires_grad=True)  # layer weights

    def forward(self, x, outputs):
        # Weights
        if self.weight:
            w = torch.sigmoid(self.w) * (2 / self.n)  # sigmoid weights (0-1)
            x = x * w[0]

        # Fusion
        nx = x.shape[1]  # input channels
        for i in range(self.n - 1):
            a = outputs[self.layers[i]] * w[i + 1] if self.weight else outputs[self.layers[i]]  # feature to add
            na = a.shape[1]  # feature channels

            # Adjust channels
            if nx == na:  # same shape
                x = x + a
            elif nx > na:  # slice input
                x[:, :na] = x[:, :na] + a  # or a = nn.ZeroPad2d((0, 0, 0, 0, 0, dc))(a); x = x + a
            else:  # slice feature
                x = x + a[:, :nx]

        return x


#class Mish(nn.Module):# https://github.com/digantamisra98/Mish
#    def forward(self, x):
#        return x * F.softplus(x).tanh()


class Swish(nn.Module):
    def forward(self, x):
        return x * torch.sigmoid(x)


class FeatureConcat(nn.Module):
    def __init__(self, layers):
        super(FeatureConcat, self).__init__()
        self.layers = layers  # layer indices
        self.multiple = len(layers) > 1  # multiple layers flag

    def forward(self, x, outputs):
        return torch.cat([outputs[i] for i in self.layers], 1) if self.multiple else outputs[self.layers[0]]




class YOLOLayer(nn.Module):
    def __init__(self, anchors, nc, img_size, yolo_index, layers, stride):
        super(YOLOLayer, self).__init__()
        self.anchors = torch.Tensor(anchors)
        self.index = yolo_index  # index of this layer in layers
        self.layers = layers  # model output layer indices
        self.stride = stride  # layer stride
        self.nl = len(layers)  # number of output layers (3)
        self.na = len(anchors)  # number of anchors (3)
        self.nc = nc  # number of classes (2)
        #print("nc")
        #print(nc)
        self.no = nc + 5  # number of outputs (85)
        self.nx, self.ny, self.ng = 0, 0, 0  # initialize number of x, y gridpoints
        self.anchor_vec = self.anchors / self.stride
        self.anchor_wh = self.anchor_vec.view(1, self.na, 1, 1, 2)

        #if ONNX_EXPORT:
        #    self.training = False
        #    self.create_grids((img_size[1] // stride, img_size[0] // stride))  # number x, y grid points

    def create_grids(self, ng=(13, 13), device='cpu'):
        self.nx, self.ny = ng  # x and y grid size
        self.ng = torch.tensor(ng, dtype=torch.float)

        # build xy offsets
        if not self.training:
            yv, xv = torch.meshgrid([torch.arange(self.ny, device=device), torch.arange(self.nx, device=device)])
            self.grid = torch.stack((xv, yv), 2).view((1, 1, self.ny, self.nx, 2)).float()

        if self.anchor_vec.device != device:
            self.anchor_vec = self.anchor_vec.to(device)
            self.anchor_wh = self.anchor_wh.to(device)

    def forward(self, p, out):
        bs, _, ny, nx = p.shape  # bs, 255, 13, 13
        if (self.nx, self.ny) != (nx, ny):
            self.create_grids((nx, ny), p.device)

        # p.view(bs, 255, 13, 13) -- > (bs, 3, 13, 13, 85)  # (bs, anchors, grid, grid, classes + xywh)
        p = p.view(bs, self.na, self.no, self.ny, self.nx).permute(0, 1, 3, 4, 2).contiguous()  # prediction

        io = p.sigmoid()
        io[..., :2] = (io[..., :2] * 2. - 0.5 + self.grid)
        io[..., 2:4] = (io[..., 2:4] * 2) ** 2 * self.anchor_wh
        io[..., :4] *= self.stride
            #io = p.clone()  # inference output
            #io[..., :2] = torch.sigmoid(io[..., :2]) + self.grid  # xy
            #io[..., 2:4] = torch.exp(io[..., 2:4]) * self.anchor_wh  # wh yolo method
            #io[..., :4] *= self.stride
            #torch.sigmoid_(io[..., 4:])
        return io.view(bs, -1, self.no), p  # view [1, 3, 13, 13, 85] as [1, 507, 85]


def get_yolo_layers(model):
    return [i for i, m in enumerate(model.module_list) if m.__class__.__name__ == 'YOLOLayer']  # [89, 101, 113]

"""
def model_info(model, verbose=False):
    # Plots a line-by-line description of a PyTorch model
    n_p = sum(x.numel() for x in model.parameters())  # number parameters
    n_g = sum(x.numel() for x in model.parameters() if x.requires_grad)  # number gradients
    #if verbose:
    #    print('%5s %40s %9s %12s %20s %10s %10s' % ('layer', 'name', 'gradient', 'parameters', 'shape', 'mu', 'sigma'))
    #    for i, (name, p) in enumerate(model.named_parameters()):
    #        name = name.replace('module_list.', '')
    #        print('%5g %40s %9s %12g %20s %10.3g %10.3g' %
    #              (i, name, p.requires_grad, p.numel(), list(p.shape), p.mean(), p.std()))

    try:  # FLOPS
        from thop import profile
        flops = profile(deepcopy(model), inputs=(torch.zeros(1, 3, 64, 64),), verbose=False)[0] / 1E9 * 2
        fs = ', %.1f GFLOPS' % (flops * 100)  # 640x640 FLOPS
    except:
        fs = ''

    print('Model Summary: %g layers, %g parameters, %g gradients%s' % (len(list(model.parameters())), n_p, n_g, fs))
"""


def load_image_local(path,img_size):
    # loads 1 image from dataset, returns img, original hw, resized hw
    #img = img_path
        #path = self.img_files[index]
    img = cv2.imread(path)  # BGR
    assert img is not None, 'Image Not Found ' + path
    h0, w0 = img.shape[:2]  # orig hw
    r = img_size / max(h0, w0)  # resize image to img_size
    if r != 1:  # always resize down, only resize up if training with augmentation
        interp = cv2.INTER_AREA if r < 1 and not False else cv2.INTER_LINEAR
        img = cv2.resize(img, (int(w0 * r), int(h0 * r)), interpolation=interp)
    return img, (h0, w0), img.shape[:2]  # img, hw_original, hw_resized


def non_max_suppression_local(prediction, conf_thres=0.1, iou_thres=0.6, merge=False, classes=None, agnostic=False):
    """Performs Non-Maximum Suppression (NMS) on inference results

    Returns:
         detections with shape: nx6 (x1, y1, x2, y2, conf, cls)
    """
    if prediction.dtype is torch.float16:
        prediction = prediction.float()  # to FP32

    nc = prediction[0].shape[1] - 5  # number of classes
    xc = prediction[..., 4] > conf_thres  # candidates

    # Settings
    min_wh, max_wh = 2, 4096  # (pixels) minimum and maximum box width and height
    max_det = 300  # maximum number of detections per image
    time_limit = 10.0  # seconds to quit after
    redundant = True  # require redundant detections
    multi_label = nc > 1  # multiple labels per box (adds 0.5ms/img)

    t = time.time()
    output = [None] * prediction.shape[0]
    for xi, x in enumerate(prediction):  # image index, image inference
        # Apply constraints
        # x[((x[..., 2:4] < min_wh) | (x[..., 2:4] > max_wh)).any(1), 4] = 0  # width-height
        x = x[xc[xi]]  # confidence

        # If none remain process next image
        if not x.shape[0]:
            continue

        # Compute conf
        x[:, 5:] *= x[:, 4:5]  # conf = obj_conf * cls_conf

        # Box (center x, center y, width, height) to (x1, y1, x2, y2)
        box = xywh2xyxy(x[:, :4])

        # Detections matrix nx6 (xyxy, conf, cls)
        if multi_label:
            i, j = (x[:, 5:] > conf_thres).nonzero(as_tuple=False).T
            x = torch.cat((box[i], x[i, j + 5, None], j[:, None].float()), 1)
        else:  # best class only
            conf, j = x[:, 5:].max(1, keepdim=True)
            x = torch.cat((box, conf, j.float()), 1)[conf.view(-1) > conf_thres]

        # Filter by class
        if classes:
            x = x[(x[:, 5:6] == torch.tensor(classes, device=x.device)).any(1)]

        # Apply finite constraint
        # if not torch.isfinite(x).all():
        #     x = x[torch.isfinite(x).all(1)]

        # If none remain process next image
        n = x.shape[0]  # number of boxes
        if not n:
            continue

        # Sort by confidence
        # x = x[x[:, 4].argsort(descending=True)]

        # Batched NMS
        c = x[:, 5:6] * (0 if agnostic else max_wh)  # classes
        boxes, scores = x[:, :4] + c, x[:, 4]  # boxes (offset by class), scores
        i = torchvision.ops.boxes.nms(boxes, scores, iou_thres)
        if i.shape[0] > max_det:  # limit detections
            i = i[:max_det]

        output[xi] = x[i]
        if (time.time() - t) > time_limit:
            break  # time limit exceeded

    return output


def letterbox_local(img, new_shape=(640, 640), color=(114, 114, 114), auto=True, scaleFill=False, scaleup=True):
    # Resize image to a 32-pixel-multiple rectangle https://github.com/ultralytics/yolov3/issues/232
    shape = img.shape[:2]  # current shape [height, width]
    if isinstance(new_shape, int):
        new_shape = (new_shape, new_shape)

    # Scale ratio (new / old)
    r = min(new_shape[0] / shape[0], new_shape[1] / shape[1])
    if not scaleup:  # only scale down, do not scale up (for better test mAP)
        r = min(r, 1.0)

    # Compute padding
    ratio = r, r  # width, height ratios
    new_unpad = int(round(shape[1] * r)), int(round(shape[0] * r))
    dw, dh = new_shape[1] - new_unpad[0], new_shape[0] - new_unpad[1]  # wh padding
    #if auto:  # minimum rectangle
    #    dw, dh = np.mod(dw, 64), np.mod(dh, 64)  # wh padding
    #elif scaleFill:  # stretch
    #    dw, dh = 0.0, 0.0
    #    new_unpad = (new_shape[1], new_shape[0])
    #    ratio = new_shape[1] / shape[1], new_shape[0] / shape[0]  # width, height ratios

    dw /= 2  # divide padding into 2 sides
    dh /= 2

    if shape[::-1] != new_unpad:  # resize
        img = cv2.resize(img, new_unpad, interpolation=cv2.INTER_LINEAR)
    top, bottom = int(round(dh - 0.1)), int(round(dh + 0.1))
    left, right = int(round(dw - 0.1)), int(round(dw + 0.1))
    img = cv2.copyMakeBorder(img, top, bottom, left, right, cv2.BORDER_CONSTANT, value=color)  # add border
    return img, ratio, (dw, dh)


def output_to_target_local(output, width, height):
    # Convert model output to target format [batch_id, class_id, x, y, w, h, conf]
    if isinstance(output, torch.Tensor):
        output = output.cpu().numpy()

    targets = []
    for i, o in enumerate(output):
        if o is not None:
            for pred in o:
                box = pred[:4]
                w = (box[2] - box[0]) / width
                h = (box[3] - box[1]) / height
                x = box[0] / width + w / 2
                y = box[1] / height + h / 2
                conf = pred[4]
                cls = int(pred[5])

                targets.append([i, cls, x, y, w, h, conf])

    return np.array(targets)


def xywh2xyxy(x):
    # Convert nx4 boxes from [x, y, w, h] to [x1, y1, x2, y2] where xy1=top-left, xy2=bottom-right
    y = torch.zeros_like(x) if isinstance(x, torch.Tensor) else np.zeros_like(x)
    y[:, 0] = x[:, 0] - x[:, 2] / 2  # top left x
    y[:, 1] = x[:, 1] - x[:, 3] / 2  # top left y
    y[:, 2] = x[:, 0] + x[:, 2] / 2  # bottom right x
    y[:, 3] = x[:, 1] + x[:, 3] / 2  # bottom right y
    return y



def parse_args():
    parser = argparse.ArgumentParser(description='Use list download video then decode into images')
    parser.add_argument('--weights', type=str, default='./weights/crowdcounting_eyelevel_withoutcfg.pth', help='model.pt path(s) /model path')
    parser.add_argument('--img-size', type=int, default=320, help='inference size (pixels)')
    parser.add_argument('--conf-thres', type=float, default=0.4, help='object confidence threshold')
    parser.add_argument('--iou-thres', type=float, default=0.48, help='IOU threshold for NMS')
    parser.add_argument('--device', default='0', help='cuda device, i.e. 0 or 0,1,2,3 or cpu')
    parser.add_argument('--c-model', dest='c_model', default=False, action='store_true', help='generate c++ model')
    parser.add_argument('--debug', dest='debug', default=False, action='store_true', help='generate c++ model')
    args = parser.parse_args()
    return args



if __name__ == '__main__':

    args = parse_args()
    testing_img_list = glob.glob('./input_img/*')

    if args.c_model:

        input_shape = [1, 3, args.img_size, args.img_size]

        # generate gpu c++ model
        device = select_device("0")
        # ONNX_EXPORT = False("default is not exporting ONNX format, plz let me know if using ONNX is easier")
        #model = Darknet(args.cfg_file).to(device)
        #ckpt = torch.load(args.weights, map_location=device)
        #model.load_state_dict(ckpt['model'], strict=False)
        model = torch.load(args.weights)
        model.eval()
        model.cuda()

        output_model_name_GPU = './weights/model_C_GPU.pt'
        example = (torch.rand(input_shape) * 255).cuda(0)
        example = example.contiguous().view(1, 3, args.img_size, args.img_size)
        traced_script_module = torch.jit.trace(model.cuda(0), example, check_trace=False)
        #traced_script_module = torch.jit.trace(model.cuda(0), example, check_trace=False)

        if args.debug:
            print("start debug gpu c++ model")
            t_pp_1 = time.time()
            for i in range(10):
                # example = (torch.rand(input_shape) * 255).cuda(0)
                example = (torch.rand(1, 3, args.img_size, args.img_size)).cuda(0)
                output_online, train_out = model(example, augment=False)
                # output_online = model.module(example)
                # output_online=output_online[0]
                output_traced = traced_script_module(example)[0]
                for i in range(len(output_online) - 1):
                    if torch.all(torch.lt(torch.abs(torch.add(output_traced[i], -output_online[i])), 1e-5)) == False:
                        print(
                            'torch.all(torch.lt(torch.abs(torch.add(output_traced[i], -output_online[i])), 1e-5))==False')
            print('gpu c++ model Convertion time', time.time() - t_pp_1)

        traced_script_module.save(output_model_name_GPU)



        # generate cpu c++ model
        output_model_name_CPU = './weights/model_C_CPU.pt'
        device = torch.device('cpu')
        # ONNX_EXPORT = False("default is not exporting ONNX format, plz let me know if using ONNX is easier")
        #model = Darknet(args.cfg_file).to(device)
        #ckpt = torch.load(args.weights, map_location=device)
        #model.load_state_dict(ckpt['model'], strict=False)
        model = torch.load(args.weights)
        model.cpu()
        model.eval()
        example = (torch.rand(input_shape) * 255).cpu()
        example = example.contiguous().view(1, 3, args.img_size, args.img_size)
        traced_script_module = torch.jit.trace(model.cpu(), example, check_trace=False)

        if args.debug:
            print("start debug cpu c++ model")
            t_pp_1 = time.time()
            for i in range(10):
                # example = (torch.rand(input_shape) * 255).cpu()
                example = (torch.rand(1, 3, args.img_size, args.img_size)).cpu()
                # with torch.no_grad():
                output_online, train_out = model(example, augment=False)
                # output_online = model.module(example)
                # output_online=output_online[0]
                output_traced = traced_script_module(example)[0]
                for i in range(len(output_online) - 1):
                    if torch.all(torch.lt(torch.abs(torch.add(output_traced[i], -output_online[i])), 1e-5)) == False:
                        print(
                            'torch.all(torch.lt(torch.abs(torch.add(output_traced[i], -output_online[i])), 1e-5))==False')
            print('cpu c++ model Convertion time', time.time() - t_pp_1)

        traced_script_module.save(output_model_name_CPU)




    device = select_device(args.device)

    #ONNX_EXPORT = False("default is not exporting ONNX format, plz let me know if using ONNX is easier")

    #model = Darknet(args.cfg_file).to(device)
    #ckpt = torch.load(args.weights, map_location=device)

    #model.load_state_dict(ckpt['model'], strict=False)
    model = torch.load(args.weights)
    model.eval()

    print("strat local process")



    for images in testing_img_list:

        img, (h0, w0), (h, w) = load_image_local(images, args.img_size)

        img, ratio2, pad2 = letterbox_local(img, (args.img_size, args.img_size), auto=False, scaleup=False)

        img = img[:, :, ::-1].transpose(2, 0, 1)  # BGR to RGB, to 3x416x416
        img = np.ascontiguousarray(img)
        img = torch.from_numpy(img)

        img = img.contiguous().view(1, 3, args.img_size, args.img_size)

        img = img.to(device, non_blocking=True)
        img = img.float()  # uint8 to fp16/32
        img /= 255.0  # 0 - 255 to 0.0 - 1.0
        nb, nb_channels, height, width = img.shape  # batch size, channels, height, width
        whwh = torch.Tensor([width, height, width, height]).to(device)

        with torch.no_grad():
            inf_out, train_out = model(img, augment=False)  # inference and training outputs
            output = non_max_suppression_local(inf_out, conf_thres=args.conf_thres, iou_thres=args.iou_thres,
                                               merge=False)

        # print("output")
        # print(output)

        sample_data = output_to_target_local(output, width, height)
        sample_data_1 = [item for item in sample_data if item[1] == 1]  # only take label==1 which is human body
        sample_data_1 = [item for item in sample_data_1 if item[6] > args.conf_thres]  # take score above theshold

        sample_data_0_newplot = []
        for boxs in sample_data_1:
            # new_line=[]
            xmin = boxs[2] - boxs[4] / 2
            xmin = xmin.cpu().numpy().astype(float).item()
            ymin = boxs[3] - boxs[5] / 2
            ymin = ymin.cpu().numpy().astype(float).item()
            xmax = boxs[2] + boxs[4] / 2
            xmax = xmax.cpu().numpy().astype(float).item()
            ymax = boxs[3] + boxs[5] / 2
            ymax = ymax.cpu().numpy().astype(float).item()
            conf = boxs[6].cpu().numpy().astype(float).item()
            gt = numpy.int(boxs[1])
            new_line = [xmin, ymin, xmax, ymax, conf, gt]
            #take detected body in [xmin, ymin, xmax, ymax, conf, class/label] format
            sample_data_0_newplot.append(new_line)

        data_for_plot = [images, sample_data_0_newplot]

        print("image path")
        print(data_for_plot[0])
        print("result [xmin, ymin, xmax, ymax, conf, class/label] for all deteced box is ")
        print(data_for_plot[1])

        #write box to txt file in/result/ folder
        fname = "./result/" + data_for_plot[0].split("/")[-1].replace(".png", ".txt").replace(".jpg", ".txt")
        print("generete result txt file")
        print(fname)
        with open(fname, 'w') as f:
            f.write("xmin,ymin,xmax,ymax,score,class" + '\n')
            for boxes in data_for_plot[1]:
                f.write(str(boxes[0]) + "," + str(boxes[1]) + "," + str(boxes[2]) + "," + str(boxes[3]) + "," + str(
                    boxes[4]) + "," + str(boxes[5]) + "," + '\n')











